const fs = require('fs');
const path = require('path');

const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = relativePath => path.resolve(appDirectory, relativePath);

const paths = {
    publicPath:     '/',
    appSrc:         resolveApp('app/js'),
    appIndexJs:     resolveApp('app/js/index.js'),
    appBuild:       resolveApp('build'),
    appHtml:        resolveApp('public/index.html'),
    appNodeModules: resolveApp('node_modules'),
    appPublic:      resolveApp('public/static'),
};

module.exports = paths;
