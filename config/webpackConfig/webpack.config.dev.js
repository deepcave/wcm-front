const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');

require('dotenv').config();

const paths = require('../paths');

const NODE_ENV = process.env.NODE_ENV || 'prod';
const URL = process.env.URL || '127.0.0.1';
const PORT = process.env.PORT || '8080';

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const publicPath = '/';

const env = {
    NODE_ENV: NODE_ENV,
};

// This is the development configuration.
// It is focused on developer experience and fast rebuilds.
module.exports = {
    devtool:     'cheap-module-source-map',
    entry: {
        app: [
            'react-dev-utils/webpackHotDevClient',
            'react-hot-loader/patch',
            `webpack-dev-server/client?http://${URL}:${PORT}`,
            'webpack/hot/only-dev-server',
            paths.appIndexJs,
        ],
        
    },

    output:      {
        path:                          paths.appBuild,
        pathinfo:                      true,
        // Not a real file. It's the virtual path that is served
        // by WebpackDevServer in development.
        filename:                      'static/js/[name].js',
        chunkFilename:                 'static/js/[id].js',
        // This is the URL that app is served from.
        publicPath,
        // Point sourcemap entries to original disk location
        devtoolModuleFilenameTemplate: info => path.resolve(info.absoluteResourcePath),
    },
    resolve:     {
        // This allows you to set a fallback for where Webpack should look for modules.
        // We placed these paths second because we want `node_modules` to "win"
        // if there are any conflicts. This matches Node resolution mechanism.
        modules:    ['node_modules', paths.appNodeModules],
        // JSX is not recommended, see:
        // https://github.com/facebookincubator/create-react-app/issues/290
        extensions: ['.js', '.json', '.jsx'],
        // Enable aliases to use utils as separate packages.
        // We might convert project to monorepo in the future.
        // https://github.com/lerna/lerna
        alias:      {
            Packages: path.resolve(__dirname, '../../app/js/packages'),
            components: path.resolve(__dirname, '../../app/js/components'),
            pages: path.resolve(__dirname, '../../app/js/pages'),
            actions: path.resolve(__dirname, '../../app/js/actions'),
            utils: path.resolve(__dirname, '../../app/js/utils'),
        },
        symlinks: false,
    },
    module:      {
        // Makes missing exports an error instead of warning.
        strictExportPresence: true,
        rules:                [
            require('./rules/files'),
            require('./rules/images'),
            require('./rules/js'),
            require('./rules/css'),
            require('./rules/styl'),
            require('./rules/less'),
        ],
    },
    plugins:     [
        require('./definePlugin')(NODE_ENV),
        // Generates an `index.html` file with the <script> injected.
        new HtmlWebpackPlugin({
            inject:   true,
            template: paths.appHtml,
            filename: paths.appBuild + '/index.html',
            chunks: ['app'],
        }),
        new webpack.DefinePlugin(env),
        // This is necessary to emit hot updates (currently CSS only):
        new CaseSensitivePathsPlugin(),
        new webpack.NamedModulesPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.HotModuleReplacementPlugin(),
    ],
    node:        {
        fs:  'empty',
        net: 'empty',
        tls: 'empty',
    },
    // Turn off performance hints during development because we don't do any
    // splitting or minification in interest of speed. These warnings become
    // cumbersome.
    performance: {
        hints: false,
    },
};