const config = require('./webpack.config.dev');
const paths = require('../paths');

module.exports = function (proxy, allowedHost) {
    return {
        //disableHostCheck:   !proxy,
        // Enable gzip compression of generated files.
        compress:           true,
        // Silence WebpackDevServer's own logs since they're generally not useful.
        // It will still show compile warnings and errors with this setting.
        clientLogLevel:     'none',
        contentBase:        paths.appPublic,
        //watchContentBase:   true,
        hot:                true,
        publicPath:         config.output.publicPath,
        quiet:              true,
        watchOptions:       {
            ignored: /node_modules/,
        },
        overlay: true,
        historyApiFallback: {
            // Paths with dots should still use the history fallback.
            // See https://github.com/facebookincubator/create-react-app/issues/387.
            disableDotRule: true,
        },
        //public:             allowedHost,
        proxy,
    };
};