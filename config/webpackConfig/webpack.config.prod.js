const path = require('path');
const fs = require('fs');
const webpack = require('webpack');

const NODE_ENV = process.env.NODE_ENV || 'prod';

require('dotenv').config();

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');

const paths = require('../paths');

if (process.env.NODE_ENV !== 'production') {
    throw new Error('Production builds must have NODE_ENV=production.');
}

const HTML_PLUGIN_MINIFY_OPTIONS = {
    removeComments:                true,
    collapseWhitespace:            true,
    removeRedundantAttributes:     true,
    useShortDoctype:               true,
    removeEmptyAttributes:         true,
    removeStyleLinkTypeAttributes: true,
    keepClosingSlash:              true,
    minifyJS:                      true,
    minifyCSS:                     true,
    minifyURLs:                    true,
};

// This is the development configuration.
// It is focused on developer experience and fast rebuilds.
module.exports = {
    // Don't attempt to continue if there are any errors.
    bail:    true,
    devtool: 'source-map',
    entry:   {
        app: [require.resolve('../polyfills'), paths.appIndexJs],
    },
    output:  {
        path:                          paths.appBuild,
        // There will be one main bundle, and one file per asynchronous chunk.
        filename:                      'static/js/[name].[chunkhash:8].js',
        chunkFilename:                 'static/js/[name].[chunkhash:8].chunk.js',
        // This is the URL that app is served from.
        publicPath:                    paths.publicPath,
        // Point sourcemap entries to original disk location
        devtoolModuleFilenameTemplate: info =>
                                           path.resolve(info.absoluteResourcePath),
    },
    resolve: {
        // JSX is not recommended, see:
        // https://github.com/facebookincubator/create-react-app/issues/290
        extensions: ['.js', '.json', '.jsx'],
        alias: {
            Packages: path.resolve(__dirname, '../../app/js/packages'),
            components: path.resolve(__dirname, '../../app/js/components'),
            pages: path.resolve(__dirname, '../../app/js/pages'),
            actions: path.resolve(__dirname, '../../app/js/actions'),
            utils: path.resolve(__dirname, '../../app/js/utils'),
        },
        symlinks: false,
    },
    module:  {
        // Makes missing exports an error instead of warning.
        strictExportPresence: true,
        rules:                [
            require('./rules/files'),
            require('./rules/images'),
            require('./rules/js'),
            require('./rules/css'),
            require('./rules/styl'),
            require('./rules/less'),
        ],
    },
    plugins: [
        require('./definePlugin')(NODE_ENV),
        new webpack.optimize.ModuleConcatenationPlugin(),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new ExtractTextPlugin({
            filename: '[name].css',
            allChunks: true,
        }),
        // Generates an `index.html` file with the <script> injected.
        new HtmlWebpackPlugin({
            inject:   true,
            template: paths.appHtml,
            filename: paths.appBuild + '/index.html',
            chunks: ['app'],
            minify:   HTML_PLUGIN_MINIFY_OPTIONS,
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress:  {
                warnings:     false,
                sequences:    true,
                booleans:     true,
                loops:        true,
                unused:       true,
                drop_console: true,
                unsafe:       true,
                // Disabled because of an issue with Uglify breaking seemingly valid code:
                // https://github.com/facebookincubator/create-react-app/issues/2376
                // Pending further investigation:
                // https://github.com/mishoo/UglifyJS2/issues/2011
                comparisons:  false,
            },
            output:    {
                comments: false,
            },
            sourceMap: false,
            beautify:  false,
            comments:  false,
            screw_ie8: true,
            minimize:  true,
        }),
    ],
    stats:   'minimal',
    node:    {
        fs:  'empty',
        net: 'empty',
        tls: 'empty',
    },
};