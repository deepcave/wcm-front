// The "file" loader handles all assets unless explicitly excluded.
// The `exclude` list *must* be updated with every change to loader extensions.
module.exports = {
    exclude: [
        /\.html$/,
        /\.(js|jsx)$/,
        /\.(css|styl|less)$/,
        /\.json$/,
        /\.bmp$/,
        /\.gif$/,
        /\.jpe?g$/,
        /\.png$/,
    ],
    loader:  require.resolve('file-loader'),
    options: {
        name: 'static/media/[name].[hash:8].[ext]',
    },
};