const autoprefixer = require('autoprefixer');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const theme = {
    //'primary-color': '#09ce95',
};

const postcssOptions = {
    sourceMap: 'inline',
    ident: 'postcss', // https://webpack.js.org/guides/migrating/#complex-options
    plugins: () => [
        require('postcss-flexbugs-fixes'),
        autoprefixer({
            browsers: [
                '>1%',
                'last 4 versions',
                'Firefox ESR',
                'not ie < 9', // React doesn't support IE8 anyway
            ],
            flexbox: 'no-2009',
        }),
    ],
};

const devConfig = {
    test: /\.less$/,
    use: [
        require.resolve('style-loader'),
        {
            loader: require.resolve('css-loader'),
            options: {
                importLoaders: 1,
            },
        },
        {
            loader:  require.resolve('postcss-loader'),
            options: postcssOptions,
        },
        {
            loader: require.resolve('less-loader'),
            options: {
                sourceMap: true,
                modifyVars: theme,
            },
        },
    ],
};

const prodConfig = {
    test: /\.less$/,
    loader: ExtractTextPlugin.extract(
        Object.assign({
            fallback: require.resolve('style-loader'),
            use: [
                {
                    loader: require.resolve('css-loader'),
                    options: {
                        importLoaders: 1,
                        minimize: true,
                        sourceMap: true,
                    },
                },
                {
                    loader:  require.resolve('postcss-loader'),
                    options: postcssOptions,
                },
                {
                    loader: require.resolve('less-loader'),
                    options: {
                        sourceMap: true,
                        modifyVars: theme,
                    },
                },
            ],
        })
    )
};

module.exports = process.env.NODE_ENV === 'dev'
    ? devConfig
    : prodConfig;
