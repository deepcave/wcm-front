const autoprefixer = require('autoprefixer');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = process.env.NODE_ENV === 'dev'
    ? {
        test: /\.styl$/,
        use:  [
            require.resolve('style-loader'),
            {
                loader:  require.resolve('css-loader'),
                options: {
                    importLoaders: 1,
                },
            },
            {
                loader:  require.resolve('postcss-loader'),
                options: {
                    sourceMap: 'inline',
                    ident:     'postcss', // https://webpack.js.org/guides/migrating/#complex-options
                    plugins:   () => [
                        require('postcss-flexbugs-fixes'),
                        autoprefixer({
                            browsers: [
                                '>1%',
                                'last 4 versions',
                                'Firefox ESR',
                                'not ie < 9', // React doesn't support IE8 anyway
                            ],
                            flexbox:  'no-2009',
                        }),
                    ],
                },
            },
            require.resolve('stylus-loader'),
        ],
    }
    : {
        test:   /\.styl$/,
        loader: ExtractTextPlugin.extract(
            Object.assign(
                {
                    fallback: require.resolve('style-loader'),
                    use:      [
                        {
                            loader:  require.resolve('css-loader'),
                            options: {
                                importLoaders: 1,
                                minimize:      true,
                                sourceMap:     true,
                            },
                        },
                        {
                            loader:  require.resolve('postcss-loader'),
                            options: {
                                // Necessary for external CSS imports to work
                                // https://github.com/facebookincubator/create-react-app/issues/2677
                                ident:   'postcss',
                                plugins: () => [
                                    require('postcss-flexbugs-fixes'),
                                    autoprefixer({
                                        browsers: [
                                            '>1%',
                                            'last 4 versions',
                                            'Firefox ESR',
                                            'not ie < 9', // React doesn't support IE8 anyway
                                        ],
                                        flexbox:  'no-2009',
                                    })
                                ]
                            }
                        },
                        require.resolve('stylus-loader')
                    ]
                }
            )
        )
        // Note: this won't work without `new ExtractTextPlugin()` in `plugins`.
    };