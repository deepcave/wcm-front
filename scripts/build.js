// Packages
const webpack = require('webpack');
const path = require('path');
const fs = require('fs-extra');
const recursive = require('recursive-readdir');
const chalk = require('chalk');
const gzipSize = require('gzip-size').sync;

// Ours
const config = require('../config/webpackConfig/webpack.config.prod');
const formatWebpackMessages = require('../utils/formatWebpackMessages');
const paths = require('../config/paths');

// Empty build dir and merge it with resource files.
fs.emptyDirSync(paths.appBuild);
copyPublicFolder();

// Build the app.
build().then(({stats, warnings}) => {
    if (warnings.length) {
        console.log(chalk.yellow('Compiled with warnings.\n'));
        console.log(warnings.join('\n\n'));
        console.log(
            '\nSearch for the ' +
            chalk.underline(chalk.yellow('keywords')) +
            ' to learn more about each warning.'
        );
    } else {
        printFileSizesAfterBuild(stats);
        console.log(chalk.green('Compiled successfully.\n'));
    }
});

// Create the production build and print the deployment instructions.
function build() {
    console.log('Creating an optimized production build...');

    let compiler = webpack(config);
    return new Promise((resolve, reject) => {
        compiler.run((err, stats) => {
            if (err) {
                return reject(err);
            }
            const messages = formatWebpackMessages(stats.toJson({}, true));
            if (messages.errors.length) {
                return reject(new Error(messages.errors.join('\n\n')));
            }
            return resolve({
                stats,
                warnings: messages.warnings,
            });
        });
    });
}

function copyPublicFolder() {
    fs.copySync(paths.appPublic, paths.appBuild, {
        dereference: true,
        filter:      file => file !== paths.appHtml,
    });
}

function formatSize(size) {
    return Number(size / 1024).toFixed(2) + ' Kb';
}

function printFileSizesAfterBuild(webpackStats) {
    const root = paths.appBuild;
    const assets = webpackStats
        .toJson()
        .assets.filter(asset => /\.(js|css)$/.test(asset.name))
        .map(asset => {
            const fileContents = fs.readFileSync(path.join(root, asset.name));
            const size = gzipSize(fileContents);
            return {
                folder: path.join(path.basename(paths.appBuild), path.dirname(asset.name)),
                name:   path.basename(asset.name),
                size:   size,
            };
        });

    assets.forEach(asset => {
        console.log(
            '  ' + (formatSize(asset.size)) +
            '  ' + chalk.dim(asset.folder + path.sep) +
            chalk.cyan(asset.name)
        );
    });
}
