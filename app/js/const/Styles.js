export const ConstStyles = {
    badgeWithShadow:  {backgroundColor: '#fff', color: '#666', boxShadow: '0 0 0 1px #d9d9d9 inset'},
    chartColors:      ['#09CE95', '#444', '#90ED7D', '#29BCD6', '#FF511F', '#29BCD6'],
    oneTextLine:      {textOverflow: 'ellipsis', overflow: 'hidden', whiteSpace: 'nowrap'},
    thirdColumn:      {padding: 20},
    defaultCardStyle: {minHeight: 135},
};