export const URL_PARTS = {
};

export const HOME = '/';

export const WMC  = '/wmc';
export const WMC_LOG  = '/log';
export const WMC_PROFILE = '/me';
export const WMC_CONFIG = '/config';