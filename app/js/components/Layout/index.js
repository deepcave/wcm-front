import {Button, Icon, Layout, Menu} from 'antd';
import React, {PureComponent} from 'react';
import {Link, Route} from 'react-router-dom';
import {HOME,WMC,WMC_LOG} from '../../const/MenuConstants';
import Home from '../../pages/Home';
import WMCLog from '../../pages/WMCLog';
import WmcStore from '../../stores/WmcStore';
import './Layout.styl';

const {Header, Content, Footer} = Layout;

class AppLayout extends PureComponent {

    componentWillMount() {

    }

    onLogout = _ => {
    };

    render() {
        return (
            <Layout className="wmc-layout" style={{overflow:"hidden"}}>
                <Header>
                    <Link to={HOME}>
                        <div className="logo" onClick={()=>{WmcStore.loadData()}}/>
                    </Link>
                    <Menu
                        theme="dark"
                        mode="horizontal"
                        defaultSelectedKeys={['1']}
                        style={{lineHeight: '24px'}}
                    >
                        <Menu.Item key="1">Terminal</Menu.Item>
                        <Menu.Item key="2">Store</Menu.Item>
                        <Menu.Item key="3">Log</Menu.Item>
                    </Menu>
                </Header>
                <Content style={{padding: '0 50px'}}>
                    <div className="content-wrapper">
                        <Route path={HOME} component={Home} exact/>
                        <Route path={WMC}  component={Home} exact/>
                        <Route path={WMC_LOG} component={WMCLog} exact/>
                        
                        {/*<Route component={() => <Redirect to={HOME}/>}/>*/}
                    </div>
                </Content>
                {/*<Footer style={{textAlign: 'center'}}>
                    Error Report UI # {__VERSION__} ©2018
                </Footer>*/}
            </Layout>
        );
    }
}

export default AppLayout;

const styles = {
    version: {
        marginTop:      32,
        color:          '#313a4b',
        flex:           1,
        display:        'flex',
        justifyContent: 'center',
        alignItems:     'flex-end',
    },
};
