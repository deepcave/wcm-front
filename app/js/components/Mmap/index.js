import {observer} from 'mobx-react';
import * as React from "react";
import WmcStore from '../../stores/WmcStore';
import WmcMapStore,{exitD,exitE,exitN,exitS,exitU,exitW} from '../../stores/WmcMapStore';
import './Mmap.styl';

class MmapCell extends React.Component {
    
    componentDidMount() {
        
    }

    render() {
        let cell = WmcMapStore.getMapCell(this.props.x,this.props.y);
        let cellStyle = {};
        if (cell[exitN]) {
            cellStyle.borderTop="1px solid blue";
        }
        if (cell[exitS]) {
            cellStyle.borderBottom="1px solid blue";
        }
        if (cell[exitW]) {
            cellStyle.borderLeft="1px solid blue";
        }
        if (cell[exitE]) {
            cellStyle.borderRight="1px solid blue";
        }
        let key = this.props.x+"-"+this.props.y;
        return <div className="wmc-map-cell" key={key} style={cellStyle}></div>;
    }
}

class MmapLine extends React.Component {
    
    render() {
        let cols = [];
        let elKey = this.props.elKey || "m0";        
        for (let x=0;x<=20;x++) {
            let itKey = elKey+"-"+x;
            cols.push(<MmapCell y={this.props.y} x={x} key={itKey}/>);
        }
        return <div className="wmc-map-line" key={elKey}>{cols}</div>;
    }
}

@observer
class Mmap extends React.Component {

    componentDidMount() {
        //WmcStore.store.setMap(this);
    }

    render() {
        if (!WmcStore.isMapVisible()) return [];
        let lines = [];
        for (let y=0;y<20;y++) {
            lines.push (<MmapLine key={"map-line"+y} y={y} version={WmcMapStore.store.mapVersion}/>);
        }
        console.log("render map");
        return(
            <div className="wmc-map">
                {lines}
            </div>
        );
    }
}

export default Mmap;