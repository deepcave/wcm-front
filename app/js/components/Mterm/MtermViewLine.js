import { Component } from 'react';
import * as React from 'react';

export class MtermViewText extends Component {

    render() {
        let k = 2048;
        let content = [];
        for (let part of this.props.text) {
            content.push(<MtermViewLine line={part} keyPart={k++}/>);
            content.push(<br/>);
        }
        return (<div>{content}</div>);
    }
}

export class MtermViewLine extends Component {

    render() {
        const parts = this.props.line;
        const kp = this.props.keyPart?this.props.key:"0";
        let content = [];
        let i = 0;
        for (let part of parts) {
            i++;
            let key = kp+"-"+i;
            content.push(<span key={key} style={{color:part.fg,background:part.bg}}>{part.text}</span>)
        }
        return (<span>{content}</span>);
    }
}

export default MtermViewLine;