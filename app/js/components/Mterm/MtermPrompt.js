import {observer} from 'mobx-react';
import * as React from "react";
import { Component } from 'react';
import WmcPromptStore from '../../stores/WmcPromptStore';
import MtermViewLine from './MtermViewLine';

const MtermInputChar = (char,active,index) => <span className={active?"mterm-prompt-cursor":"mterm-prompt-input"} key={index}>{char}</span>;

class MtermPromptInput extends Component {
    render() {
        let i = 0;
        return (<span>
            {this.props.value.buffer.map((letter) => (MtermInputChar(letter!=" "?letter:"\u00A0",(i++)==this.props.value.cursor,i)))}
            {i==this.props.value.cursor?MtermInputChar("\u00A0",true,i):''}
        </span>);
    }
}

@observer
class MtermPrompt extends Component {
    render() {
        return(
            <div className="mterm-prompt">
                <span style={{color:"yellow"}}>#</span><MtermViewLine nobr={true} line={WmcPromptStore.getPrompt()}/>
                <MtermPromptInput value={WmcPromptStore.getPromptImput()}/>
            </div>
        );
    }
}

export default MtermPrompt;