import {observer} from 'mobx-react';
import * as React from "react";
import WmcStore from '../../stores/WmcStore';
import { Component } from 'react';

class MtermStatusLine extends Component {

    render() {
        let version = this.props.version;
        let data = WmcStore.getStatus(this.props.line);
        let content = [];
        let i = 0;
        for (let obj of data) {
            content.push(<span key={"status-line-"+this.props.line+"-"+i++}>[{obj.value}]</span>);
        }
        return (
            <div key={"status-line-"+this.props.line} className="mterm-status-line" version={version}>{content}</div>
        );
    }
}

@observer
class MtermStatus extends Component {
    
    componentDidMount() {
        WmcStore.setStatus(0,"main","login","пользователь");
        WmcStore.setStatus(0,"main","connected","подключен");
        WmcStore.setStatus(1,"main","line");
    }

    render() {
        let status = WmcStore.getStatusVersion();
        return(
            <div className="mterm-status">
                <MtermStatusLine key="status-0" version={WmcStore.getStatusVersion()} line={0}/>
                <MtermStatusLine key="status-1" ersion={WmcStore.getStatusVersion()} line={1}/>              
            </div>
        );
    }
}

export default MtermStatus;