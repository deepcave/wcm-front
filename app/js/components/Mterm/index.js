import {observer} from 'mobx-react';
import * as React from "react";
import WmcStore from '../../stores/WmcStore';
import MtermView from './MtermView';
import MtermPrompt from './MtermPrompt';
import MtermStatus from './MtermStatus';
import './Mterm.styl';
import { Component } from 'react';

class Mterm extends React.Component {

    render() {
        return(
            <div className="mterm-cont">
                <MtermView/>
                <MtermPrompt/>
                <MtermStatus/>
            </div>
        );
    }
}

export default Mterm;