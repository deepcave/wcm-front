import { Component } from 'react';
import {observer} from 'mobx-react';
import * as React from "react";
import WmcViewStore from '../../stores/WmcViewStore';
import {MtermViewText,MtermViewLine} from './MtermViewLine';

@observer
class MtermView extends Component {
    
    componentDidMount() { 
        
    }

    render() {
        let mtermLines = [];
        let i=4095;
        for (let text of WmcViewStore.getViewLines(256)) {
            i++;
            mtermLines.push(
                <MtermViewLine key={"meterm-view-"+i} line={text}/>
            );
        }
        return (<div className="mterm-view">
            {mtermLines}
        </div>);
    }
}

export default MtermView;
