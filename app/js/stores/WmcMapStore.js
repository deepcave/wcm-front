import {action, observable} from 'mobx';
import axios from 'axios';
import WmcSocket from '../utils/WmcSocket';
//import WmcViewStore from './WmcViewStore';
//import WmcPromptStore from './WmcPromptStore';

export const exitN = 0;
export const exitS = 1;
export const exitW = 2;
export const exitE = 3;
export const exitD = 4;
export const exitU = 5;
export const data = 6;


export function roomData () {
    //      N S W E D U  I
    return [0,0,0,0,0,0,{}];
}

export class ZoneData {
    constructor() {
        this.rooms = {};
    }

    getRoom(vnum) {
        if (!rooms[vnum]) {
            return false;
        }
    }
    addRoom(room) {
        if (!room instanceof(RoomData)) {
            console.log("map store error, roomData fuckup");
            return false;
        }
    }
}

export class WorldData {
    constructor() {
        this.zones = {};
    }
}

export class RoomData {

    constructor() {
        this.exits = {};
        this.vnum = 0;
        this.name = "неизвестная комната";
    }

    setExit(exit,room) {
        if (!room) {
            this.exits[exit] = false;
        }
    }
}

class WmcMapStore {
    @observable store;

    constructor() {
        this.store = {
            mapVersion: 0,
            mapC: 0,
        };
        this.zones = {};        
        this.currentZone = "47";
        this.currentRoom = "4706";
    }
    setExit(map,x,y,e) {
        if (!map[x]) map[x]={};
        if (!map[x][y]) map[x][y]={};
        if (!map[x][y][e]) map[x][y][e]=1;
    }
    rmake(minimap,x,y,vn) {
        if (minimap.was[vn]) return;
        minimap.was[vn]=1;
        let room = minimap.g[vn];
        console.log("go go",x,y);
        minimap.cor[vn]=[x,y];
        if (room.e) {//запад
            this.setExit(minimap.map,x,y,'e');
            this.rmake(minimap,x+1,y,room.e.vn);
        }
        if (room.w) {//запад
            this.setExit(minimap.map,x,y,'w');
            this.rmake(minimap,x-1,y,room.w.vn);
        }
        if (room.n) {//запад
            this.setExit(minimap.map,x,y,'n');
            this.rmake(minimap,x,y-1,room.n.vn);
        }
        if (room.s) {//запад
            this.setExit(minimap.map,x,y,'s');
            this.rmake(minimap,x,y+1,room.s.vn);
        }
    }

    mkZone(zoneVnum) {
        let G={};
        for (let vnum in this.zones[zoneVnum].Rooms) {
            G[vnum]={"vn":vnum};
        }
        console.log("mk zone");
        let start = ""
        for (let vnum in this.zones[zoneVnum].Rooms) {
            start = vnum;
            for(let direct in this.zones[zoneVnum].Rooms[vnum].Exits) {
                let eVnum = this.zones[zoneVnum].Rooms[vnum].Exits[direct];
                G[vnum][direct]=G[eVnum]||null;
            }
        }
        let x=8,y=8;
        let miniMap = {};
        miniMap.was = {};
        miniMap.map = {};
        miniMap.cor = {};
        miniMap.g = G;
        this.rmake(miniMap,8,8,start);
        this.zones[zoneVnum].g = G;
        this.bmap = miniMap;
    }


    getMapCell(x,y) {
        if (!this.bmap) return roomData();
        if (!this.bmap.map[x]) return roomData();
        if (!this.bmap.map[x][y]) return roomData();
        let room1 = [
            this.bmap.map[x][y]["n"]?0:1,
            this.bmap.map[x][y]["s"]?0:1,
            this.bmap.map[x][y]["w"]?0:1,
            this.bmap.map[x][y]["e"]?0:1,
        ];
        let room = [1,1,1,1];
        console.log(x,y,room1);
        return room1;
    }

    setZoneData(strData) {
        if (!strData) return;
        let data = JSON.parse(strData);
        if (!data) return;
        this.zones[data.Vnum]=data;
        this.mkZone(data.Vnum);
        console.log(this.bmap);
        this.store.mapVersion++;        
    }
}

export default new WmcMapStore();