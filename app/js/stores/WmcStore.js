import {action, observable} from 'mobx';
import axios from 'axios';
import WmcSocket from '../utils/WmcSocket';
import WmcViewStore from './WmcViewStore';
import WmcPromptStore from './WmcPromptStore';


//const sendUrl = "https://mud.x-ix.ru/wcm/api/send/sholod/";

const trgregexp = /^\{([^}]*)\}\s*\{([^}]*)\}\s*$/;
const cmds = {
    '/action': (opts) => {
        let o = null;
        if (o = opts.match(trgregexp)) {
        } else {
            WmcViewStore.printLn("\ue00112bad trigger options : \ue00113" + opts);
        }
    },
    '/map' : (opts) => {

    },
    '/ping':  (opts) => {

    },
    '/login': (opts) => {

    }
};

class WmcStore {
    @observable store;

    constructor() {
        this.store = {
            auth: false,
            statusVersion: 0,
            varVersion: 0,
        };
        this.data = {
            vars: {},
            status : [[],[]],
        };
        WmcSocket.setMainStore(this);
    }

    isMapVisible() {
        return true;
    }

    runCmd(line) {
        let [cmd,...args] = line.split(' ');
        switch (cmd) {
            case '/echo':
                WmcViewStore.printLn(args.join( ));
                break;
            case '/var':
                WmcViewStore.printLn("\u000325Пример\u000352Новый");
            break;
            case '/action':
                cmds[cmd](args.join(" "));
            break;
            case '/login':
                WmcSocket.login(args);
            break;
            case '/msdp':
                WmcSocket.sendMSDPToServer(args);
            break;
            case '/map': 
                WmcSocket.sendMap(args);
            break;
            default:
            break;
        }
    }

    getMapCell(x,y) {
        let room = [0,0,0,0,0,0];
        if (x==5) {room[exitN]=1;room[exitS]=1;}
        if (x==4 && y==5) {room[2]=room[3]=1;}
        if (x==5 && y==5) {room[0]=0;};
        return room;
    }

    getConnection() {
        return "Ok";
    }

    setVar(mode,name,value) {
        if (!this.data.vars[mode]) {
            this.data.vars[mode] = {};
        }
        if (!this.data.vars[mode][name]) {
            this.data.vars[mode][name] = {};
        }
        this.data.vars[mode][name].value = value;
        this.data.vars[mode][name].name = name;
        this.data.vars[mode][name].mode = mode;
        this.store.varVersion++;
        this.store.statusVersion++;
    }
    
    getVar(mode,name) {
        if (!this.data[mode]) return false;
        if (!this.data[mode][name]) return false;
        return this.data.vars[mode][name];
    }

    setStatus(line,mode,name) {
        if (!this.getVar(mode,name)) {
            this.setVar(mode,name,"");
        }
        this.data.status[line].push(this.data.vars[mode][name]);
    }

    getStatus(line) {
        return this.data.status[line];
    }

    getVersion() {
        return this.store.varVersion;
    }
    getStatusVersion() {
        return this.store.statusVersion*1000;
    }
}

export default new WmcStore();
