import {action, observable} from 'mobx';
import WmcSocket from '../utils/WmcSocket';
import ansiParser from '../utils/AnsiParser';
import WmcStore from '../stores/WmcStore';

class WmcPromptStore {
    @observable store;

    constructor() {
        this.cursorPosition = 0;
        this.buffer = [];
        this.store = {
            fake:1,
            prompt: [],
            buffer: [],
            promptLine: '',
        };
        this.history = [];
        this.historyPosition = 0;
        this.lastLine = 0;
        WmcSocket.setPromptStore(this);
        //ул;в;с;з
        window.onpaste = (ev) => {
            console.log(123321,"PASTE",ev);
        };
        window.addEventListener('paste',(ev)=>{
            let text = ev.clipboardData.getData('Text');
            if (!text) return;
            console.log("gona paste ",this,text);
            for (let i = 0;i<text.length;i++) {
                if (text[i]=="\n") {
                    this.enter();
                    continue;
                }
                this.addCharToLine(text[i]);
            }
        });
        window.onkeydown = (ev)=> {
            const {ctrlKey,key} = ev;
            if (key == 'Insert') {
                console.log(ev);
                return true;
            }
            for (let p of ['Alt','Shift','Control']) {
                if (p==key) return false;
            }
            if (key == "Unidentified") {
                return true;
            }
            if (ev.ctrlKey && ev.altKey) {
                console.log("ctr+alt+",ev);
                return false;
            }
            if (ev.ctrlKey) {
                if (ev.code == 'KeyV') {
                    return true;
                }
                console.log("ctr+",ev);
                return false;
            }
            if (ev.altKey) {
                console.log("alt+",ev);
                return false;
            }
            //TODO:добавить обработчик хоткеев
            switch (key) {//Обработчик редактора строки
                case 'ArrowLeft':
                case 'Left':
                    if (this.cursorPosition>0) this.cursorPosition--;
                    this.updateCursor();
                break;
                case 'ArrowRight':
                case 'Right':
                    if (this.cursorPosition<this.buffer.length) this.cursorPosition++;
                    this.updateCursor();
                break;
                case 'Home':
                    this.cursorPosition=0;
                    this.updateCursor();
                break;
                case 'End':
                    this.cursorPosition=this.buffer.length;
                    this.updateCursor();
                break;
                case 'Backspace':
                    this.delChar(-1);
                break;
                case 'Delete':
                    this.delChar(1);
                break;
                case 'Enter':
                    this.enter();
                break;
                case 'ArrowUp':
                case 'Up':
                    let newHistoryPosition = this.historyPosition+1;
                    if (!this.history[this.history.length-newHistoryPosition]) {
                        return;
                    }
                    let offset = this.history.length - newHistoryPosition;
                    this.historyPosition = newHistoryPosition;
                    this.setLine(this.history[offset]);
                break;
                case 'ArrowDown':
                    this.searchNext(-1,this.buffer.join(''));
                break;
                default:
                    this.addCharToLine(key);
                break;
            }
            this.updateCursor();
            return false;
        };
    }

    searchNext(direct,line) {
    }

    updateCursor() {
        let start = 0;
        let end =this.buffer.length - 1;
        this.update();
    }
    
    _getBufferBeforeCursor() {
        if (this.cursorPosition==0) return [];
        return this.store.buffer.slice(0,this.cursorPosition);
    }
    _getBufferAfterCursor() {
        if (this.cursorPosition == this.buffer.length) return [];
        return this.store.buffer.slice(this.cursorPosition,this.buffer.length);
    }

    reset() {
        this.buffer = [];
        this.cursorPosition = 0;
        this.update();
    }

    enter() {
        const line = this.buffer.join('');
        this.reset();
        if ( line && line != "" )this.history.push(line);
        this.historyPosition = 0;
        for (let part of line.split(';')) {
            if (part[0] && part[0]=='/') {
                WmcStore.runCmd(part);
            } else {
                WmcSocket.sendLineToServer(part);
            }
        }
    }

    delChar(d) {
        let pre = this._getBufferBeforeCursor();
        let post= this._getBufferAfterCursor();
        if (d<0) {
            if (this.cursorPosition==0) return;
            if (pre.pop()) {
                this.cursorPosition--;
            }
        } else {
            post.shift();
        }
        this.buffer = [...pre,...post];
        this.update();
    }

    update() {
        const newStoreBuffer = [...this.buffer];
        this.store.fake++;
        this.store.buffer = newStoreBuffer;
    }

    setLine(line) {
        if (!line) return;
        let newByffer = [];
        for (let i=0;i<line.length;i++) {
            newByffer.push(line[i]);
        }
        this.buffer = newByffer;
        this.update();
    }
    addCharToLine(char) {
        this.buffer.splice(this.cursorPosition,0,char);
        this.cursorPosition++;
        this.update();    
    }

    getPrompt() {
        return this.store.prompt;
    }
    
    getPromptImput() {
        let result = {
            buffer: [...this.store.buffer],
            cursor: this.cursorPosition
        };
        return result;
    }

    setPrompt(data) {
        if (data.Time < this.lastTime)
            return;
        this.lastLine = data.Time;
        let line = ansiParser(data.Text);
        this.store.prompt = line;
    }
}

export default new WmcPromptStore();