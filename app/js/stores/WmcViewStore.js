import {parserAnsiLine,parserAnsiText,colorLine} from '../utils/AnsiParser';
import {action, observable} from 'mobx';
import WmcSocket from '../utils/WmcSocket';


class WmcViewStore {
    @observable store;

    constructor() {
        this.store = {
            lineBuffer: []
        };
        this.lastTime = 0;
        this.linePreBuffer = [];
        this.queueLine = [];
        WmcSocket.setViewStore(this);
    }

    reset() {
        console.log("RESET TERMINAL");
        let newBuffer=[];
        this.linePreBuffer = [];
        this.store.lineBuffer = [];
        this.addLine({Time:0,Line:"Terminal reset"});
        this.flush();
    }

    flush() {
        let newBuffer = [...this.store.lineBuffer,...this.linePreBuffer];
        this.linePreBuffer = [];
        this.store.lineBuffer = newBuffer;
    }


    addText(data) {
        if (data.Time < this.lastTime) {
            console.log("object time error");
            return;
        }
        this.queueLine.push(data);
        if (this.lockView) {
            console.log("collision on add data");
            return;
        } else {
            this.lockView = true;
            let co;
            while(co = this.queueLine.shift()) {
                let buffer = parserAnsiText(co.Text);
                for (let line of buffer) {
                    this.linePreBuffer.push(line);
                }
            }
            this.lockView = false;
        }
        this.flush();
    }

    addLine(data) {
        this.addText({Time:data.Time,Text:data.Line});
        return;
    }

    printLn(line) {
        this.linePreBuffer.push(colorLine(line));
        this.flush();
    }
    getViewLines(limit) {
        let result = [];
        let start = this.store.lineBuffer.length > limit ? this.store.lineBuffer.length - limit : 0;
        for (let line of this.store.lineBuffer.slice(start)) {
            result.push(line);
        }
        return result;
    }
}

export default new WmcViewStore();