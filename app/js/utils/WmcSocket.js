import WmcStore from '../stores/WmcStore';
import WmcMapStore from '../stores/WmcMapStore';
import io from 'socket.io-client';

class WmcSocket {
    viewStore = null;
    promptStore = null;
    mainStore = null;

    constructor() {
        let prompt = false;
        this.socket = io("https://mud.x-ix.ru", {path:"/wcm/api/socket.io"});
        
        this.userName = 'sholod';
        this.userPassword = '123321';
        this.socket.on('connect', () => {
            console.log("SOCKET CONNECT");
            WmcStore.setVar("main","connected","connected");
        });

        this.socket.on('authRequest',(request) => {
            console.log("Server request auth ",request);
            if (this._viewStore) this._viewStore.reset();
            if (this._promptStore) this._promptStore.reset();
            if (this.userName && this.userPassword) {
                this.login([this.userName,this.userPassword]);
            }
        });

        this.socket.on('authResponse',(response) => {
            if (this.viewStore) this.viewStore.reset();
            if (this.promptStore) this.promptStore.reset();
            console.log("AUTH RESPONSE",response);
            WmcStore.setVar("main","login",response);
        });

        this.socket.on("zoneMap",(data)=>{
           WmcMapStore.setZoneData(data);
        });

        this.socket.on('disconnect', (data) => {
            WmcStore.setVar("main","connected","not connected");
        });

        this.socket.on('newMSDP', data => {
            //console.log(JSON.parse(data));
        });

        this.socket.on('newText', data => {
            if (this.viewStore != null) {
                this.viewStore.addText(data);
            }
            
        });

        this.socket.on('newPrompt', data => {
            this.viewStore.addText(data);
            if (this.promptStore != null) {
                this.promptStore.setPrompt(data);
            }
            WmcStore.setVar("main","line",data.Time);
        });

        this.socket.on('connect_error', error => {
            console.log("sio error",error);
        });
    
        this.socket.on('reconnect', (attemptNumber) => {
            console.log("reconnect");
        });
    }

    setPromptStore(_promptStore) {
        this.promptStore = _promptStore;
    }

    setViewStore(_viewStore) {
        this.viewStore = _viewStore;
    }

    setMainStore(_mainStore) {
        this.mainStore = _mainStore;
    }

    login([userName,userPassword]) {
        this.userName = userName;
        this.userPassword = userPassword;
        this.socket.emit("auth",this.userName,this.userPassword);
    }
    sendMap([zone]) {
        this.socket.emit("getMap",zone);
    }
    sendMSDPToServer([varName,varValue]) {
        this.socket.emit("sendMSDP",varName,varValue);
    }

    sendLineToServer(line) {
        console.log("SOCKET SEND LINE",line);
        this.socket.emit("sendLine",line);
    }
}

export default new WmcSocket();