
//разбор ANSI строк в массив <span>
const ansiPattern = /^[^m]+m/;
const spacePattern = /\s/g;
const spaceFirst = /^\s/g;
const space2Pattern = /(\S)\u00A0(\S)/g;
const ansiColor = ['gray','red','green','yellow','blue','magneta','cyan','#aaa'];
const mSGI = "\ue001";
export function colorLine(text) {
    let result = [];
    let parts =  text.split(mSGI);
    if (parts.length==1) return [{"fg":'#aaa',"bg":'black',"text":text}];
    for (let part of parts) {
        result.push({
            "fg":ansiColor[part.substr(0,1)],
            "bg":ansiColor[part.substr(1,1)],
            "text":part.substr(2)
        });
    }
    return result;
}

export function parserAnsiText(text) {
    let result = [];
    let colors = {fg:'#aaa',bg:'black',extra:0};
    for (let line of text.split(/\r?\n/)) {
        result.push(parserAnsiLine(line,colors));
    }
    return result;
}

export function parserAnsiLine(line,colors) {
    
    //Цвета поумолчанию
    if (!colors) {
        colors = {fg:'#aaa',bg:'black',extra:0};
    }
    //Массив для результата    

    let lObject = [];
    //Разбили строчку на блоки с цветами

    let lParts = line.split("\u001B[");
    
    if (lParts.length == 0) {
        //Строчка - пустая
        return [];
    } else if (lParts.length == 1) {
        //Если  кусочек один, то строка в цвете "поумолчанию"
        //if (line == "") return []; //Это если строка пустая (ну вот)
        lObject.push({"fg":colors.fg,"bg":colors.bg,"text":line});
    } else {
        //Если кусочков больше одного - строчка цветная
        for (let part of lParts) {
            if (part=="") continue; //если кусок пустой - бежим дальше
            let ansi = "";
            for (let i=0;i<part.length&&part[i]!='m';i++) ansi+=part[i];
            //удаляем ANSI
            let text = part.replace(ansiPattern,'');
            //Заменяем все пробелы на неразрывные
            text = text.replace(spacePattern,"\u00A0");
            //заменяем одиночные пробелы на разрывные, что бы "  " было не " "
            //text = text.replace(space2Pattern,"$1 $2");
            //заменяем первый пробел на неразрыный
            //text = text.replace(spaceFirst,'\u00A0');
            for (let ct of ansi.split(";")) {
                if (ct==0) {
                    colors.fg = '#aaa';
                    colors.bg = 'black';
                }
                if (ct==1) {
                    colors.extra = 1;
                }
                let color = ansiColor[ct%10];
                switch(Math.floor(ct/10)) {
                    case 3:
                    colors.fg=color;
                    break;
                    case 4:
                    colors.bg=color;
                    break;
                }
            }
            lObject.push({"fg":colors.fg,"bg":colors.bg,"extra":colors.extra,"text":text});
        } 
    }
    return (lObject);
};

export default parserAnsiLine;
