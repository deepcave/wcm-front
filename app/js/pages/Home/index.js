import * as React from "react";
import {observer} from 'mobx-react';
import Mterm from 'components/Mterm';
import Mmap from 'components/Mmap';
import WmcStore from '../../stores/WmcStore';
import './Home.styl';

export default class Home extends React.Component {

    render() {
        return <div className="wmc-home">   
            <Mmap/>
            <Mterm/>
        </div>;
    }
}
