import {LocaleProvider} from 'antd';
import React, {PureComponent} from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Layout from './components/Layout';

function version() {
    return __VERSION__;
}

window.version = version;

export default class App extends PureComponent {
    render() {
        return (
                <div>
                    <Router>
                        <Route exact path="*" component={Layout}/>
                    </Router>
                </div>
        );
    }
}

