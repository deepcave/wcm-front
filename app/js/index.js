import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {AppContainer} from 'react-hot-loader';
import App from './App';

const rootEl = document.getElementById('app-mount');
const render = (Component, props) =>
    ReactDOM.render(
        <AppContainer>
            <Component {...props} />
        </AppContainer>,
        rootEl,
    );

render(App, {});

if (module.hot) {
    module.hot.accept('./App', () => {
        render(App, {});
        console.log('Hot!');
    });
}