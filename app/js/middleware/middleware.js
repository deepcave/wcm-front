import {Modal, notification} from 'antd';
import React from 'react';
import {FormattedMessage as Fm} from 'react-intl';
import {stopSubmit} from 'redux-form';
import {
    redirectToLogin,
    RESPONSE_CODE_HEADER,
    RESPONSE_MESSAGE_HEADER,
    RESPONSE_SUCCESS_FLAG_HEADER,
} from '../utils/ApiUtils';

export const openNotificationWithIcon = (type, message, duration = 3) => {
    let title = message.title ? message.title : message;
    let description = message.description ? message.description : null;
    const show = notification[type];
    if (show) show({message: title, description, duration});
};

const middleware = store => next => action => {
    if (action.type !== 'PROMISE') return next(action);
    const {
        actions: [startAction, successAction, failureAction],
        additionalData,
        successMessage,
        successCallback,
        errorMessage,
        errorCallback,
        delaySuccess = 0,
    } = action;

    store.dispatch({type: startAction, additionalData});

    action.promise().then((response) => {
            const {headers, data} = response;

            // Geocode request hack
            if (additionalData && additionalData.geocodeRequest && data.results) {
                return store.dispatch({
                    type:    successAction,
                    payload: data.results.length ? data.results[0] : {},
                    additionalData,
                });
            }

            const backendErrorMessage = getBackendErrorMessage(headers);
            if (!backendErrorMessage) {
                let result = Array.isArray(data) ? data : data.result;
                // When deleting was unsuccessfull, response will be [false].
                // We need to show an error in this case.
                if (result[0] === false && (!additionalData || !additionalData.geocodeRequest)) {
                    store.dispatch({type: failureAction, payload: 'Error while deleting entry.', additionalData});
                    if (errorMessage) {
                        openNotificationWithIcon('error', errorMessage, 7);
                        if (additionalData && additionalData.formName) {
                            store.dispatch(stopSubmit(additionalData.formName, result));
                        }
                    }
                    return;
                }
                setTimeout(() =>
                    store.dispatch({
                        type: successAction,
                        payload: result,
                        additionalData,
                    }),
                    delaySuccess
                );

                if (successMessage) openNotificationWithIcon('success', successMessage);
                if (typeof successCallback === 'function') successCallback(result);
            } else {
                // Catch backend error from response headers.
                store.dispatch({type: failureAction, payload: {backendErrorMessage, data}, additionalData});
                if (errorCallback) errorCallback(data);
                const errorMsg = (
                    backendErrorMessage && typeof backendErrorMessage !== 'object'
                        ? backendErrorMessage
                        : errorMessage || 'Network error.'
                );
                if (errorMsg) {
                    openNotificationWithIcon('error', errorMsg, 7);
                }
                if (additionalData && additionalData.formName) {
                    store.dispatch(stopSubmit(additionalData.formName, typeof backendErrorMessage === 'object'
                        ? backendErrorMessage : data));
                }
            }
        },
        (error) => {
            if (error.response) {
                // **Always** show error message if request was unsuccessful.
                // In order of priority:
                // 1. Client error message (from the action)
                // 2. HTTP status message (error.response.statusText)
                // 3. Error in msp response (error.response.data.message)
                // 4. Generic error message (hard-coded)
                const {statusText, data: {message}} = error.response;
                const errorMsg = (
                    errorMessage ||
                    statusText ||
                    (message && ('Network error: ' + message)) ||
                    'Network error.'
                );

                if (error.response.status === 401) {
                    showUnauthorisedModal();
                    return;
                }
                store.dispatch({type: failureAction, payload: errorMsg, additionalData});

                openNotificationWithIcon('error', errorMsg, 7);
                if (typeof errorCallback === 'function') errorCallback(errorMsg);
            }
        });
};

export default middleware;

const showUnauthorisedModal = () => {
    Modal.warning({
        title:  'You are not authorized!',
        okText: 'Go to Login page',
        onOk() {
            redirectToLogin();
        },
    });
};